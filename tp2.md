# TP2 : Explorer et manipuler le système

## 1. Une machine xubuntu fonctionnelle

## 2. Nommer la machine

➜ **On va renommer la machine**

On désignera la machine par le nom `node1.tp2.linux`

🌞 **Changer le nom de la machine**

- **première étape : changer le nom tout de suite, jusqu'à ce qu'on redémarre la machine**
  ```bash
  val@val-VirtualBox:~/Desktop$ sudo hostname node1.tp2.linux
  voal@nde1:~/Desktop$
  ```
- **deuxième étape : changer le nom qui est pris par la machine quand elle s'allume**
    ```bash
  val@node1:~/Desktop$ sudo nano /etc/hostname
  val@node1:~/Desktop$ cat /etc/hostname
    node1.tp2.linux
    ```

🌞 **Config réseau fonctionnelle**

- dans le compte-rendu, vous devez me montrer...
  ```bash
  val@node1:~$ ping 1.1.1.1
  PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
  64 bytes from 1.1.1.1: icmp_seq=1 ttl=54 time=144 ms
  3 packets transmitted, 3 received, 0% packet loss, time 2003ms
  rtt min/avg/max/mdev = 54.975/92.785/143.747/37.415 ms
  ```
  ```bash
  val@node1:~$ ping ynov.com
  PING ynov.com (92.243.16.143) 56(84) bytes of data.
  64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=50 time=99.9 ms2 packets transmitted, 2 received, 0% packet loss, time 1001ms
  rtt min/avg/max/mdev = 99.917/113.101/126.286/13.184 ms
  ```
  ```bash
  C:\Users\bravo>ping 192.168.56.106
  Envoi d’une requête 'Ping'  192.168.56.106 avec 32 octets de données :
  Réponse de 192.168.56.106 : octets=32 temps<1ms TTL=64
  Statistiques Ping pour 192.168.56.106:
  Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
  Durée approximative des boucles en millisecondes :
  Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
  ```

# Partie 1 : SSH

# II. Setup du serveur SSH

## 1. Installation du serveur

🌞 **Installer le paquet `openssh-server`**

  ```bash
  val@node1:~$ sudo !!
  sudo apt install
  [sudo] password for val:
  Reading package lists... Done
  Building dependency tree
  Reading state information... Done
  0 upgraded, 0 newly installed, 0 to remove and 101 not upgraded.
  ```

## 2. Lancement du service SSH

🌞 **Lancer le service `ssh`**

  ```bash
  val@node1:~$ systemctl start ssh
  ==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ===
  Authentication is required to start 'ssh.service'.
  Authenticating as: val,,, (val)
  Password:
  ==== AUTHENTICATION COMPLETE ===
  ```
  ```bash
  val@node1:~$ systemctl status ssh
  ● node1.tp2.linux
    State: running
     Jobs: 0 queued
   Failed: 0 units
    Since: Mon 2021-10-25 16:17:46 CEST; 59min ago
   CGroup: /
           ├─user.slice
           │ └─user-1000.slice
           │   ├─user@1000.service
           │   │ ├─gsd-xsettings.service
           │   │ │ └─1268 /usr/libexec/gsd-xsettings
           │   │ ├─gvfs-goa-volume-monitor.service
           │   │ │ └─867 /usr/libexec/gvfs-goa-volume-monitor
           │   │ ├─gsd-power.service
           │   │ │ └─1240 /usr/libexec/gsd-power
           │   │ ├─xdg-permission-store.service
           │   │ │ └─1144 /usr/libexec/xdg-permission-store
           │   │ ├─gsd-sound.service
           │   │ │ └─1253 /usr/libexec/gsd-sound
           │   │ ├─gsd-rfkill.service
           │   │ │ └─1242 /usr/libexec/gsd-rfkill
           │   │ ├─gsd-usb-protection.service
           │   │ │ └─1256 /usr/libexec/gsd-usb-protection
           │   │ ├─gsd-print-notifications.service
           │   │ │ ├─1241 /usr/libexec/gsd-print-notifications
           │   │ │ └─1325 /usr/libexec/gsd-printer
           │   │ ├─evolution-calendar-factory.service
           │   │ │ └─1163 /usr/libexec/evolution-calendar-factory
           │   │ ├─gsd-a11y-settings.service
           │   │ │ └─1225 /usr/libexec/gsd-a11y-settings
           │   │ ├─gsd-wwan.service
           │   │ │ └─1266 /usr/libexec/gsd-wwan
           │   │ ├─pulseaudio.service
           │   │ │ └─765 /usr/bin/pulseaudio --daemonize=no --log-target=journal
           │   │ ├─gsd-screensaver-proxy.service
           │   │ │ └─1243 /usr/libexec/gsd-screensaver-proxy
           │   │ ├─gsd-media-keys.service
           │   │ │ └─1239 /usr/libexec/gsd-media-keys
           │   │ ├─gvfs-daemon.service
           │   │ │ ├─ 829 /usr/libexec/gvfsd
           │   │ │ ├─ 834 /usr/libexec/gvfsd-fuse /run/user/1000/gvfs -f -o big_writes
           │   │ │ └─1205 /usr/libexec/gvfsd-trash --spawner :1.3 /org/gtk/gvfs/exec_spaw/0
           │   │ ├─evolution-source-registry.service
           │   │ │ └─1155 /usr/libexec/evolution-source-registry
           │   │ ├─gvfs-udisks2-volume-monitor.service
           │   │ │ └─852 /usr/libexec/gvfs-udisks2-volume-monitor
           │   │ ├─gsd-sharing.service
           │   │ │ └─1245 /usr/libexec/gsd-sharing
           │   │ ├─init.scope
           │   │ │ ├─693 /lib/systemd/systemd --user
           │   │ │ └─694 (sd-pam)
           │   │ ├─gsd-smartcard.service
  ```

## 3. Etude du service SSH

🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du *service*
  ````bash
  val@node1:~$ systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-10-25 17:11:39 CEST; 5min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 3558 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 3559 (sshd)
      Tasks: 1 (limit: 1105)
     Memory: 1.0M
     CGroup: /system.slice/ssh.service
             └─3559 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

  oct. 25 17:11:39 node1.tp2.linux systemd[1]: Starting OpenBSD Secure Shell server...
  oct. 25 17:11:39 node1.tp2.linux sshd[3559]: Server listening on 0.0.0.0 port 22.
  oct. 25 17:11:39 node1.tp2.linux sshd[3559]: Server listening on :: port 22.
  oct. 25 17:11:39 node1.tp2.linux systemd[1]: Started OpenBSD Secure Shell server.
  ```
- afficher le/les processus liés au *service* `ssh`
  ```bash
  1000    3171  00000000  00000000  00000000 <80000040 S+   pts/0      0:00 systemctl status ssh
  ```
  ```bash
   1000    3603  00000000  00000000  00000000 <f3d1fef9 R+   pts/1      0:00 ps ssh
  ```
 
- afficher le port utilisé par le *service* `ssh`
    ```bash
    u_str LISTEN 0      128               /tmp/ssh-ubXypLNa908z/agent.916 27806                           * 0
    ```
    ```bash
    tcp   LISTEN 0      128                                       0.0.0.0:ssh                       0.0.0.0:*
    ```
     ```bash
  tcp   LISTEN 0      128                                          [::]:ssh                          [::]:*
  ```
  - isolez uniquement la/les ligne(s) intéressante(s)
- afficher les logs du *service* `ssh`
  ```bash
  val@node1:~$ journalctl
-- Logs begin at Mon 2021-10-25 16:05:33 CEST, end at Wed 2021-10-27 09:55:03 CEST. --
oct. 25 16:05:33 val-VirtualBox kernel: Linux version 5.11.0-38-generic (buildd@lgw01-amd64-041) (gcc >
oct. 25 16:05:33 val-VirtualBox kernel: Command line: BOOT_IMAGE=/boot/vmlinuz-5.11.0-38-generic root=>
oct. 25 16:05:33 val-VirtualBox kernel: KERNEL supported cpus:
oct. 25 16:05:33 val-VirtualBox kernel:   Intel GenuineIntel
oct. 25 16:05:33 val-VirtualBox kernel:   AMD AuthenticAMD
```
  ````bash
  val@node1:/var$ cd log/
  val@node1:/var/log$ ls
  val@node1:/var/log$ sudo cat auth.log
Oct 25 16:06:26 val-VirtualBox systemd-logind[593]: New seat seat0.
Oct 25 16:06:26 val-VirtualBox systemd-logind[593]: Watching system buttons on /dev/input/event0 (Power Button)
  ```

🌞 **Connectez vous au serveur**

- depuis votre PC, en utilisant un **client SSH**
  ```bash
  bravo@Asus-Valentin MINGW64 /$ ssh val@192.168.56.106
val@192.168.56.106's password:
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)
  ```

## 4. Modification de la configuration du serveur

🌞 **Modifier le comportement du service**

- c'est dans le fichier `/etc/ssh/sshd_config`
```bash
val@node1:~$ sudo nano /etc/ssh/sshd_config
```
- effectuez le modifications suivante :
  - changer le ***port d'écoute*** du service *SSH*
    - peu importe lequel, il doit être compris entre 1025 et 65536
  ```bash
  val@node1:~$ cat /etc/ssh/sshd_config
  #Port 1026
  ```
- pour cette modification, prouver à l'aide d'une commande qu'elle a bien pris effet
  ````bash
  LISTEN 0      128          0.0.0.0:1026        0.0.0.0:*     users:(("sshd",pid=2732,fd=3))
  ```

🌞 **Connectez vous sur le nouveau port choisi**
```bash
bravo@Asus-Valentin MINGW64 /
$ ssh val@192.168.56.106 -p 1026
val@192.168.56.106's password:
Welcome to Ubuntu 20.04.3 LTS (GNU/Linux 5.11.0-38-generic x86_64)
```
# Partie 2 : FTP

# I. Intro

# II. Setup du serveur FTP

## 1. Installation du serveur

🌞 **Installer le paquet `vsftpd`**
 ```bash
  val@node1:~$ sudo apt install vsftpd
  ```

## 2. Lancement du service FTP

🌞 **Lancer le service `vsftpd`**

```bash
val@node1:~$ systemctl start vsftpd
```
```bash
val@node1:~$ system status vsftpd

Command 'system' not found, did you mean:

  command 'system3' from deb simh (3.8.1-6)
  command 'systemd' from deb systemd (245.4-4ubuntu3.13)

Try: sudo apt install <deb name>

val@node1:~$ systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2021-10-27 11:00:05 CEST; 5min ago
   Main PID: 3097 (vsftpd)
      Tasks: 1 (limit: 1105)
     Memory: 844.0K
     CGroup: /system.slice/vsftpd.service
             └─3097 /usr/sbin/vsftpd /etc/vsftpd.conf

oct. 27 11:00:05 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
oct. 27 11:00:05 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```
## 3. Etude du service FTP

🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du service
  ```bash
  val@node1:~$ systemctl status
● node1.tp2.linux
    State: running
```
- afficher le/les processus liés au service `vsftpd`
 ```bash
 val@node1:~$ ps -ef | grep "vsftpd"
root        3097       1  0 11:00 ?        00:00:00 /usr/sbin/vsftpd /etc/vsftpd.conf
val         3777    2895  0 11:20 pts/1    00:00:00 grep --color=auto vsftpd
```
- afficher le port utilisé par le service `vsftpd`
 ```bash
 val@node1:/$ sudo cat /etc/vsftpd.conf | grep "port"
# Make sure PORT transfer connections originate from port 20 (ftp-data).
connect_from_port_20=YES
 ```
- afficher les logs du service `vsftpd`
  ```bash
  val@node1:~$ journalctl | grep "vsftpd"
oct. 27 11:00:01 node1.tp2.linux sudo[2906]:      val : TTY=pts/1 ; PWD=/home/val ; USER=root ; COMMAND=/usr/bin/apt install vsftpd
oct. 27 11:00:05 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
oct. 27 11:00:05 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```

🌞 **Connectez vous au serveur**

- depuis votre PC, en utilisant un *client FTP*
```bash
Wed Oct 27 16:06:22 2021 [pid 5146] [val] OK LOGIN: Client "::ffff:192.168.56.1"
Wed Oct 27 16:06:22 2021 [pid 5148] [val] OK UPLOAD: Client "::ffff:192.168.56.1", "/home/val/Downloads
```
```bash
Wed Oct 27 16:12:15 2021 [pid 5223] [val] OK DOWNLOAD: Client "::ffff:192.168.56.1", "/home/val/Downloads/2021-10-23_18.35.09.png", 3366087 bytes, 39029.19Kbyte/sec
```

🌞 **Visualiser les logs**

```bash
Wed Oct 27 16:12:15 2021 [pid 5223] [val] OK DOWNLOAD: Client "::ffff:192.168.56.1", "/home/val/Downloads/2021-10-23_18.35.09.png", 3366087 bytes, 39029.19Kbyte/sec
```
```bash
Wed Oct 27 16:06:22 2021 [pid 5148] [val] OK UPLOAD: Client "::ffff:192.168.56.1", "/home/val/Downloads
```

## 4. Modification de la configuration du serveur

🌞 **Modifier le comportement du service**

- c'est dans le fichier `/etc/vsftpd.conf`
- effectuez les modifications suivantes :
  - changer le port où écoute `vstfpd`
    - peu importe lequel, il doit être compris entre 1025 et 65536
  - vous me prouverez avec un `cat` que vous avez bien modifié cette ligne
- pour les deux modifications, prouver à l'aide d'une commande qu'elles ont bien pris effet
  - une commande `ss -l` pour vérifier le port d'écoute

> Vous devez redémarrer le service avec une commande `systemctl restart` pour que les changements inscrits dans le fichier de configuration prennent effet.

🌞 **Connectez vous sur le nouveau port choisi**

- depuis votre PC, avec un *client FTP*
- re-tester l'upload et le download

# Partie 3 : Création de votre propre service

# I. Intro
> ***Pour toutes les commandes tapées qui figurent dans le rendu, je veux la commande ET son résultat. S'il manque l'un des deux, c'est useless.***

# II. Jouer avec netcat

🌞 **Donnez les deux commandes pour établir ce petit chat avec `netcat`**
```bash
val@node1:~/Desktop$ nc -l -p 12345
```
```bash
val@node1:~$ nc localhost 12345
```

🌞 **Utiliser `netcat` pour stocker les données échangées dans un fichier**
```bash
val@node1:~/Desktop$ nc -l 12345 > tructp.out
```

# III. Un service basé sur netcat

**Pour créer un service sous Linux, il suffit de créer un simple fichier texte.**

Ce fichier texte :

- a une syntaxe particulière
- doit se trouver dans un dossier spécifique

Pour essayer de voir un peu la syntaxe, vous pouvez utilisez la commande `systemctl cat` sur un service existant. Par exemple `systemctl cat sshd`.

DON'T PANIC pour votre premier service j'vais vous tenir la main.

La commande que lancera votre service sera un `nc -l` : vous allez donc créer un petit chat sous forme de service ! Ou presque hehe.

## 1. Créer le service

🌞 **Créer un nouveau service**

```bash
val@node1:/etc/systemd/system$ sudo mkdir chat_tp2.service
```
- déposez-y le contenu suivant :

```bash
[Unit]
Description=Little chat service (TP2)

[Service]
ExecStart=nc -l

[Install]
WantedBy=multi-user.target
```

## 2. Test test et retest

🌞 **Tester le nouveau service**
```bash
val@node1:/etc/systemd/system$ systemctl start chat_tp2.service
Failed to start chat_tp2.service: Unit chat_tp2.serice failed to load properly: Is a directory
```
  
  - vérifier qu'il est correctement lancé avec une commande  `systemctl status`
  - vérifier avec une comande `ss -l` qu'il écoute bien derrière le port que vous avez choisi
- tester depuis votre PC que vous pouvez vous y connecter
- pour visualiser les messages envoyés par le client, il va falloir regarder les logs de votre service, sur la VM :
