# I. Setup DB
**Installer MariaDB sur la machine `db.tp5.linux`**
```bash
[val@db ~]$ sudo dnf install mariadb-server
```
**Le service MariaDB**
- lancez-le avec une commande `systemctl`
```bash
sudo systemctl start mariadb.service
```
- exécutez la commande `sudo systemctl enable mariadb` pour faire en sorte que MariaDB se lance au démarrage de la machine
```bash
[val@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service
```
- vérifiez qu'il est bien actif avec une commande `systemctl`
```bash
[val@db ~]$ systemctl  status mariadb.service
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Fri 2021-11-26 15:01:23 CET; 1min 22s ago
     Docs: man:mysqld(8)
           https://mariadb.com/kb/en/library/systemd/
 Main PID: 8735 (mysqld)
   Status: "Taking your SQL requests now..."
    Tasks: 30 (limit: 4956)
   Memory: 83.9M
   CGroup: /system.slice/mariadb.service
           └─8735 /usr/libexec/mysqld --basedir=/usr

Nov 26 15:01:22 db.tp5.linux mysql-prepare-db-dir[8632]: See the MariaDB Knowledgebase at http://mariadb.com/kb or t>
Nov 26 15:01:22 db.tp5.linux mysql-prepare-db-dir[8632]: MySQL manual for more instructions.
Nov 26 15:01:22 db.tp5.linux mysql-prepare-db-dir[8632]: Please report any problems at http://mariadb.org/jira
Nov 26 15:01:22 db.tp5.linux mysql-prepare-db-dir[8632]: The latest information about MariaDB is available at http:/>
Nov 26 15:01:22 db.tp5.linux mysql-prepare-db-dir[8632]: You can find additional information about the MySQL part at:
Nov 26 15:01:22 db.tp5.linux mysql-prepare-db-dir[8632]: http://dev.mysql.com
Nov 26 15:01:22 db.tp5.linux mysql-prepare-db-dir[8632]: Consider joining MariaDB's strong and vibrant community:
Nov 26 15:01:22 db.tp5.linux mysql-prepare-db-dir[8632]: https://mariadb.org/get-involved/
Nov 26 15:01:22 db.tp5.linux mysqld[8735]: 2021-11-26 15:01:22 0 [Note] /usr/libexec/mysqld (mysqld 10.3.28-MariaDB)>
Nov 26 15:01:23 db.tp5.linux systemd[1]: Started MariaDB 10.3 database server.
```
- déterminer sur quel port la base de données écoute avec une commande `ss`
```bash
[val@db ~]$ sudo lsof -i -P -n | grep LISTEN
mysqld    8735 mysql   21u  IPv6  43387      0t0  TCP *:3306 (LISTEN)
```
- isolez les processus liés au service MariaDB (commande `ps`)
```bash
[val@db ~]$ ps aux | grep maria
val         8936  0.0  0.1 221928  1096 pts/0    S+   15:22   0:00 grep --color=auto maria
```
**Firewall**
- pour autoriser les connexions qui viendront de la machine `web.tp5.linux`, il faut conf le firewall
```bash
[val@db ~]$ sudo firewall-cmd --permanent --add-service=mysql
Warning: ALREADY_ENABLED: mysql
success
```
## 2. Conf MariaDB
**Configuration élémentaire de la base**

- exécutez la commande `mysql_secure_installation`
```bash
[val@db ~]$ mysql_secure_installation
```
```bash
-entrer votre mot de passe root: j'entre le mot de passe root 
-mettre le mot de passe root? Oui
-supprmer les utilisateur anonymes? Oui, sinon tout le monde peut utiliser
-faire en sorte qu'o puisse se connecter en root que depuis localhost? oui, évite au mdp de se faire intercepter
-retirer la bdd test? oui, c'est demandé
-appliquer tout les changements? oui
```
**Préparation de la base en vue de l'utilisation par NextCloud**
```bash
[val@db ~]$ sudo mysql -u root -p
[sudo] password for val:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 18
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
```
```bash
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.5.1.11' IDENTIFIED BY 'meow';
de la base de donnée quiQuery OK, 0 rows affected (0.002 sec)

MariaDB [(none)]>
MariaDB [(none)]> # Création de la base de donnée qui sera utilisée par NextCloud
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
 donne tQuery OK, 1 row affected (0.001 sec)

MariaDB [(none)]>
MariaDB [(none)]> # On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.5.1.11';

# ActQuery OK, 0 rows affected (0.001 sec)

MariaDB [(none)]>
MariaDB [(none)]> # Actualisation des privilèges
MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```
## 3. Test
**Installez sur la machine `web.tp5.linux` la commande `mysql`**
```bash
[val@db ~]$ sudo dnf provides mysql
Last metadata expiration check: 0:56:56 ago on Fri 26 Nov 2021 02:53:45 PM CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Repo        : appstream
Matched from:
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
```
```bash
[val@web ~]$ sudo dnf install mysql
Last metadata expiration check: 0:02:09 ago on Fri 26 Nov 2021 04:10:02 PM CET.
Dependencies resolved.
=====================================================================================================================
 Package                           Architecture  Version                                      Repository        Size
=====================================================================================================================
Installing:
 mysql                             x86_64        8.0.26-1.module+el8.4.0+652+6de068a7         appstream         12 M
Installing dependencies:
 mariadb-connector-c-config        noarch        3.1.11-2.el8_3                               appstream         14 k
 mysql-common                      x86_64        8.0.26-1.module+el8.4.0+652+6de068a7         appstream        133 k
Enabling module streams:
 mysql                                           8.0

Transaction Summary
=====================================================================================================================
Install  3 Packages

Total download size: 12 M
Installed size: 63 M
Is this ok [y/N]: y
Downloading Packages:
(1/3): mariadb-connector-c-config-3.1.11-2.el8_3.noarch.rpm                           14 kB/s |  14 kB     00:00
(2/3): mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64.rpm                  128 kB/s | 133 kB     00:01
(3/3): mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64.rpm                         3.0 MB/s |  12 MB     00:03
---------------------------------------------------------------------------------------------------------------------
Total                                                                                2.6 MB/s |  12 MB     00:04
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                             1/1
  Installing       : mariadb-connector-c-config-3.1.11-2.el8_3.noarch                                            1/3
  Installing       : mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                    2/3
  Installing       : mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                           3/3
  Running scriptlet: mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                           3/3
  Verifying        : mariadb-connector-c-config-3.1.11-2.el8_3.noarch                                            1/3
  Verifying        : mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                           2/3
  Verifying        : mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64                                    3/3

Installed:
  mariadb-connector-c-config-3.1.11-2.el8_3.noarch             mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64
  mysql-common-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

Complete!
```
**Tester la connexion**
Apres avoir refait les précédentes étapes plusieurs fois, toujour le même résultat, impatient d'avoir la correction car j'ai du faire des erreurs,
```bash
val@web ~]$ mysql -h 10.5.1.12 -P 3306 -u nextclood -p
Enter password:
ERROR 1045 (28000): Access denied for user 'nextclood'@'10.5.1.11' (using password: YES)
```
Non mais sérieux pourquoi ;(

# II. Setup Web
## 1. Install Apache
### A. Apache
**Installer Apache sur la machine `web.tp5.linux`**
```bash
[val@web ~]$ sudo dnf install httpd
[sudo] password for val:
Rocky Linux 8 - AppStream                                                             18 kB/s | 4.8 kB     00:00
Rocky Linux 8 - BaseOS                                                                18 kB/s | 4.3 kB     00:00
Rocky Linux 8 - Extras                                                                16 kB/s | 3.5 kB     00:00
Dependencies resolved.
=====================================================================================================================
 Package                    Architecture    Version                                         Repository          Size
=====================================================================================================================
Installing:
 httpd                      x86_64          2.4.37-43.module+el8.5.0+714+5ec56ee8           appstream          1.4 M
Installing dependencies:
 apr                        x86_64          1.6.3-12.el8                                    appstream          128 k
 apr-util                   x86_64          1.6.1-6.el8.1                                   appstream          104 k
 httpd-filesystem           noarch          2.4.37-43.module+el8.5.0+714+5ec56ee8           appstream           38 k
 httpd-tools                x86_64          2.4.37-43.module+el8.5.0+714+5ec56ee8           appstream          106 k
 mod_http2                  x86_64          1.15.7-3.module+el8.5.0+695+1fa8055e            appstream          153 k
 rocky-logos-httpd          noarch          85.0-3.el8                                      baseos              22 k
Installing weak dependencies:
 apr-util-bdb               x86_64          1.6.1-6.el8.1                                   appstream           23 k
 apr-util-openssl           x86_64          1.6.1-6.el8.1                                   appstream           26 k
Enabling module streams:
 httpd                                      2.4

Transaction Summary
=====================================================================================================================
Install  9 Packages

Total download size: 2.0 M
Installed size: 5.4 M
Is this ok [y/N]: y
Downloading Packages:
Rocky Linux 8 - AppStream              104% [=======================================-]  11 kB/s | 3.2 kB     --:-- ET(1/9): apr-util-bdb-1.6.1-6.el8.1.x86_64.rpm                                          91 kB/s |  23 kB     00:00
(2/9): apr-util-1.6.1-6.el8.1.x86_64.rpm                                             289 kB/s | 104 kB     00:00
(3/9): apr-util-openssl-1.6.1-6.el8.1.x86_64.rpm                                     250 kB/s |  26 kB     00:00
(4/9): apr-1.6.3-12.el8.x86_64.rpm                                                   309 kB/s | 128 kB     00:00
(5/9): httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch.rpm             363 kB/s |  38 kB     00:00
(6/9): httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64.rpm                  417 kB/s | 106 kB     00:00
(7/9): mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64.rpm                     380 kB/s | 153 kB     00:00
(8/9): rocky-logos-httpd-85.0-3.el8.noarch.rpm                                        48 kB/s |  22 kB     00:00
(9/9): httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64.rpm                        931 kB/s | 1.4 MB     00:01
---------------------------------------------------------------------------------------------------------------------
Total                                                                                716 kB/s | 2.0 MB     00:02
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                             1/1
  Installing       : apr-1.6.3-12.el8.x86_64                                                                     1/9
  Running scriptlet: apr-1.6.3-12.el8.x86_64                                                                     1/9
  Installing       : apr-util-bdb-1.6.1-6.el8.1.x86_64                                                           2/9
  Installing       : apr-util-openssl-1.6.1-6.el8.1.x86_64                                                       3/9
  Installing       : apr-util-1.6.1-6.el8.1.x86_64                                                               4/9
  Running scriptlet: apr-util-1.6.1-6.el8.1.x86_64                                                               4/9
  Installing       : httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                    5/9
  Installing       : rocky-logos-httpd-85.0-3.el8.noarch                                                         6/9
  Running scriptlet: httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch                               7/9
  Installing       : httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch                               7/9
  Installing       : mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64                                       8/9
  Installing       : httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                          9/9
  Running scriptlet: httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                          9/9
  Verifying        : apr-1.6.3-12.el8.x86_64                                                                     1/9
  Verifying        : apr-util-1.6.1-6.el8.1.x86_64                                                               2/9
  Verifying        : apr-util-bdb-1.6.1-6.el8.1.x86_64                                                           3/9
  Verifying        : apr-util-openssl-1.6.1-6.el8.1.x86_64                                                       4/9
  Verifying        : httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                          5/9
  Verifying        : httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch                               6/9
  Verifying        : httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64                                    7/9
  Verifying        : mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64                                       8/9
  Verifying        : rocky-logos-httpd-85.0-3.el8.noarch                                                         9/9

Installed:
  apr-1.6.3-12.el8.x86_64
  apr-util-1.6.1-6.el8.1.x86_64
  apr-util-bdb-1.6.1-6.el8.1.x86_64
  apr-util-openssl-1.6.1-6.el8.1.x86_64
  httpd-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  httpd-filesystem-2.4.37-43.module+el8.5.0+714+5ec56ee8.noarch
  httpd-tools-2.4.37-43.module+el8.5.0+714+5ec56ee8.x86_64
  mod_http2-1.15.7-3.module+el8.5.0+695+1fa8055e.x86_64
  rocky-logos-httpd-85.0-3.el8.noarch

Complete!
```
**Analyse du service Apache**

- lancez le service `httpd` et activez le au démarrage
```bash
[val@web ~]$ sudo systemctl start httpd.service
[val@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```
- isolez les processus liés au service `httpd`
```bash
[val@web ~]$ sudo lsof -i -P -n | grep LISTEN
httpd     2099   root    4u  IPv6  28351      0t0  TCP *:80 (LISTEN)
httpd     2101 apache    4u  IPv6  28351      0t0  TCP *:80 (LISTEN)
httpd     2102 apache    4u  IPv6  28351      0t0  TCP *:80 (LISTEN)
httpd     2103 apache    4u  IPv6  28351      0t0  TCP *:80 (LISTEN)
```
- déterminez sur quel port écoute Apache par défaut
```bash
80
```
**Un premier test**
- ouvrez le port d'Apache dans le firewall
```bash
[val@web ~]$ sudo firewall-cmd --permanent --add-service=http
success
```
### B. PHP
**Installer PHP**
```bash
[val@web ~]$ sudo dnf install epel-release
[sudo] password for val:
Last metadata expiration check: 0:24:37 ago on Mon 29 Nov 2021 10:11:43 AM CET.
Dependencies resolved.
==============================================================================================================
 Package                      Architecture           Version                     Repository              Size
==============================================================================================================
Installing:
 epel-release                 noarch                 8-13.el8                    extras                  23 k

Transaction Summary
==============================================================================================================
Install  1 Package

Total download size: 23 k
Installed size: 35 k
Is this ok [y/N]: y
Downloading Packages:
epel-release-8-13.el8.noarch.rpm                                              313 kB/s |  23 kB     00:00
--------------------------------------------------------------------------------------------------------------
Total                                                                          85 kB/s |  23 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                      1/1
  Installing       : epel-release-8-13.el8.noarch                                                         1/1
  Running scriptlet: epel-release-8-13.el8.noarch                                                         1/1
  Verifying        : epel-release-8-13.el8.noarch                                                         1/1

Installed:
  epel-release-8-13.el8.noarch

Complete!
```
```bash
[val@web ~]$ sudo dnf update
Extra Packages for Enterprise Linux 8 - x86_64                                2.0 MB/s |  11 MB     00:05
Extra Packages for Enterprise Linux Modular 8 - x86_64                        1.0 MB/s | 958 kB     00:00
Last metadata expiration check: 0:00:01 ago on Mon 29 Nov 2021 10:37:15 AM CET.
Dependencies resolved.
Nothing to do.
Complete!
```
```bash
[val@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
Last metadata expiration check: 0:00:39 ago on Mon 29 Nov 2021 10:37:15 AM CET.
remi-release-8.rpm                                                            209 kB/s |  26 kB     00:00
Dependencies resolved.
==============================================================================================================
 Package                   Architecture        Version                        Repository                 Size
==============================================================================================================
Installing:
 remi-release              noarch              8.5-1.el8.remi                 @commandline               26 k

Transaction Summary
==============================================================================================================
Install  1 Package

Total size: 26 k
Installed size: 20 k
Is this ok [y/N]: y
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                      1/1
  Installing       : remi-release-8.5-1.el8.remi.noarch                                                   1/1
  Verifying        : remi-release-8.5-1.el8.remi.noarch                                                   1/1

Installed:
  remi-release-8.5-1.el8.remi.noarch

Complete!
```
```bash
[val@web ~]$ sudo dnf module enable php:remi-7.4
Remi's Modular repository for Enterprise Linux 8 - x86_64                     1.5 kB/s | 858  B     00:00
Remi's Modular repository for Enterprise Linux 8 - x86_64                     3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Remi's Modular repository for Enterprise Linux 8 - x86_64                     2.5 MB/s | 938 kB     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                    5.8 kB/s | 858  B     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                    3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                    354 kB/s | 2.0 MB     00:05
Dependencies resolved.
==============================================================================================================
 Package                   Architecture             Version                   Repository                 Size
==============================================================================================================
Enabling module streams:
 php                                                remi-7.4

Transaction Summary
==============================================================================================================

Is this ok [y/N]: y
Complete!
```
# install de PHP et de toutes les libs PHP requises par NextCloud
```bash
[val@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
Last metadata expiration check: 0:00:51 ago on Mon 29 Nov 2021 10:38:49 AM CET.
Package zip-3.0-23.el8.x86_64 is already installed.
Package unzip-6.0-45.el8_4.x86_64 is already installed.
Package libxml2-2.9.7-11.el8.x86_64 is already installed.
Package openssl-1:1.1.1k-4.el8.x86_64 is already installed.
Dependencies resolved.
==============================================================================================================
 Package                           Arch        Version                                   Repository      Size
==============================================================================================================
Installing:
 php74-php                         x86_64      7.4.26-1.el8.remi                         remi-safe      1.5 M
 php74-php-bcmath                  x86_64      7.4.26-1.el8.remi                         remi-safe       88 k
 php74-php-common                  x86_64      7.4.26-1.el8.remi                         remi-safe      710 k
 php74-php-gd                      x86_64      7.4.26-1.el8.remi                         remi-safe       93 k
 php74-php-gmp                     x86_64      7.4.26-1.el8.remi                         remi-safe       84 k
 php74-php-intl                    x86_64      7.4.26-1.el8.remi                         remi-safe      201 k
 php74-php-json                    x86_64      7.4.26-1.el8.remi                         remi-safe       82 k
 php74-php-mbstring                x86_64      7.4.26-1.el8.remi                         remi-safe      492 k
 php74-php-mysqlnd                 x86_64      7.4.26-1.el8.remi                         remi-safe      200 k
 php74-php-pdo                     x86_64      7.4.26-1.el8.remi                         remi-safe      130 k
 php74-php-pecl-zip                x86_64      1.20.0-1.el8.remi                         remi-safe       58 k
 php74-php-process                 x86_64      7.4.26-1.el8.remi                         remi-safe       92 k
 php74-php-xml                     x86_64      7.4.26-1.el8.remi                         remi-safe      180 k
Installing dependencies:
 checkpolicy                       x86_64      2.9-1.el8                                 baseos         345 k
 environment-modules               x86_64      4.5.2-1.el8                               baseos         420 k
 fontconfig                        x86_64      2.13.1-4.el8                              baseos         273 k
 gd                                x86_64      2.2.5-7.el8                               appstream      143 k
 jbigkit-libs                      x86_64      2.1-14.el8                                appstream       54 k
 libX11                            x86_64      1.6.8-5.el8                               appstream      610 k
 libX11-common                     noarch      1.6.8-5.el8                               appstream      157 k
 libXau                            x86_64      1.0.9-3.el8                               appstream       36 k
 libXpm                            x86_64      3.5.12-8.el8                              appstream       57 k
 libicu69                          x86_64      69.1-1.el8.remi                           remi-safe      9.6 M
 libjpeg-turbo                     x86_64      1.5.3-12.el8                              appstream      156 k
 libsodium                         x86_64      1.0.18-2.el8                              epel           162 k
 libtiff                           x86_64      4.0.9-20.el8                              appstream      187 k
 libwebp                           x86_64      1.0.0-5.el8                               appstream      271 k
 libxcb                            x86_64      1.13.1-1.el8                              appstream      228 k
 libxslt                           x86_64      1.1.32-6.el8                              baseos         249 k
 oniguruma5php                     x86_64      6.9.7.1-1.el8.remi                        remi-safe      210 k
 php74-libzip                      x86_64      1.8.0-1.el8.remi                          remi-safe       69 k
 php74-runtime                     x86_64      1.0-3.el8.remi                            remi-safe      1.1 M
 policycoreutils-python-utils      noarch      2.9-16.el8                                baseos         251 k
 python3-audit                     x86_64      3.0-0.17.20191104git1c2f876.el8.1         baseos          85 k
 python3-libsemanage               x86_64      2.9-6.el8                                 baseos         126 k
 python3-policycoreutils           noarch      2.9-16.el8                                baseos         2.2 M
 python3-setools                   x86_64      4.3.0-2.el8                               baseos         625 k
 scl-utils                         x86_64      1:2.0.2-14.el8                            appstream       46 k
 tcl                               x86_64      1:8.6.8-2.el8                             baseos         1.1 M
Installing weak dependencies:
 php74-php-cli                     x86_64      7.4.26-1.el8.remi                         remi-safe      3.1 M
 php74-php-fpm                     x86_64      7.4.26-1.el8.remi                         remi-safe      1.6 M
 php74-php-opcache                 x86_64      7.4.26-1.el8.remi                         remi-safe      275 k
 php74-php-sodium                  x86_64      7.4.26-1.el8.remi                         remi-safe       87 k

Transaction Summary
==============================================================================================================
Install  43 Packages

Total download size: 28 M
Installed size: 91 M
Is this ok [y/N]: y
Downloading Packages:
(1/43): jbigkit-libs-2.1-14.el8.x86_64.rpm                                     56 kB/s |  54 kB     00:00
(2/43): gd-2.2.5-7.el8.x86_64.rpm                                             145 kB/s | 143 kB     00:00
(3/43): libXau-1.0.9-3.el8.x86_64.rpm                                         1.1 MB/s |  36 kB     00:00
(4/43): libX11-common-1.6.8-5.el8.noarch.rpm                                  2.2 MB/s | 157 kB     00:00
(5/43): libXpm-3.5.12-8.el8.x86_64.rpm                                        1.0 MB/s |  57 kB     00:00
(6/43): libX11-1.6.8-5.el8.x86_64.rpm                                         522 kB/s | 610 kB     00:01
(7/43): libjpeg-turbo-1.5.3-12.el8.x86_64.rpm                                 1.0 MB/s | 156 kB     00:00
(8/43): libtiff-4.0.9-20.el8.x86_64.rpm                                       1.4 MB/s | 187 kB     00:00
(9/43): scl-utils-2.0.2-14.el8.x86_64.rpm                                     947 kB/s |  46 kB     00:00
(10/43): libwebp-1.0.0-5.el8.x86_64.rpm                                       2.4 MB/s | 271 kB     00:00
(11/43): libxcb-1.13.1-1.el8.x86_64.rpm                                       2.3 MB/s | 228 kB     00:00
(12/43): fontconfig-2.13.1-4.el8.x86_64.rpm                                   1.8 MB/s | 273 kB     00:00
(13/43): checkpolicy-2.9-1.el8.x86_64.rpm                                     1.6 MB/s | 345 kB     00:00
(14/43): libxslt-1.1.32-6.el8.x86_64.rpm                                      624 kB/s | 249 kB     00:00
(15/43): environment-modules-4.5.2-1.el8.x86_64.rpm                           619 kB/s | 420 kB     00:00
(16/43): python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64.rpm           550 kB/s |  85 kB     00:00
(17/43): python3-libsemanage-2.9-6.el8.x86_64.rpm                             1.4 MB/s | 126 kB     00:00
(18/43): policycoreutils-python-utils-2.9-16.el8.noarch.rpm                   419 kB/s | 251 kB     00:00
(19/43): python3-setools-4.3.0-2.el8.x86_64.rpm                               671 kB/s | 625 kB     00:00
(20/43): libsodium-1.0.18-2.el8.x86_64.rpm                                    614 kB/s | 162 kB     00:00
(21/43): tcl-8.6.8-2.el8.x86_64.rpm                                           792 kB/s | 1.1 MB     00:01
(22/43): python3-policycoreutils-2.9-16.el8.noarch.rpm                        1.5 MB/s | 2.2 MB     00:01
(23/43): php74-libzip-1.8.0-1.el8.remi.x86_64.rpm                             698 kB/s |  69 kB     00:00
(24/43): oniguruma5php-6.9.7.1-1.el8.remi.x86_64.rpm                          1.4 MB/s | 210 kB     00:00
(25/43): php74-php-bcmath-7.4.26-1.el8.remi.x86_64.rpm                        1.6 MB/s |  88 kB     00:00
(26/43): php74-php-7.4.26-1.el8.remi.x86_64.rpm                               1.6 MB/s | 1.5 MB     00:00
(27/43): php74-php-common-7.4.26-1.el8.remi.x86_64.rpm                        1.4 MB/s | 710 kB     00:00
(28/43): php74-php-cli-7.4.26-1.el8.remi.x86_64.rpm                           2.0 MB/s | 3.1 MB     00:01
(29/43): php74-php-gd-7.4.26-1.el8.remi.x86_64.rpm                            1.1 MB/s |  93 kB     00:00
(30/43): php74-php-gmp-7.4.26-1.el8.remi.x86_64.rpm                           519 kB/s |  84 kB     00:00
(31/43): php74-php-intl-7.4.26-1.el8.remi.x86_64.rpm                          1.1 MB/s | 201 kB     00:00
(32/43): php74-php-json-7.4.26-1.el8.remi.x86_64.rpm                          804 kB/s |  82 kB     00:00
(33/43): php74-php-mbstring-7.4.26-1.el8.remi.x86_64.rpm                      769 kB/s | 492 kB     00:00
(34/43): php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64.rpm                       455 kB/s | 200 kB     00:00
(35/43): php74-php-fpm-7.4.26-1.el8.remi.x86_64.rpm                           859 kB/s | 1.6 MB     00:01
(36/43): php74-php-pdo-7.4.26-1.el8.remi.x86_64.rpm                           1.0 MB/s | 130 kB     00:00
(37/43): php74-php-opcache-7.4.26-1.el8.remi.x86_64.rpm                       1.0 MB/s | 275 kB     00:00
(38/43): php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64.rpm                      1.3 MB/s |  58 kB     00:00
(39/43): php74-php-process-7.4.26-1.el8.remi.x86_64.rpm                       1.2 MB/s |  92 kB     00:00
(40/43): php74-php-sodium-7.4.26-1.el8.remi.x86_64.rpm                        1.4 MB/s |  87 kB     00:00
(41/43): php74-php-xml-7.4.26-1.el8.remi.x86_64.rpm                           3.0 MB/s | 180 kB     00:00
(42/43): php74-runtime-1.0-3.el8.remi.x86_64.rpm                              2.1 MB/s | 1.1 MB     00:00
(43/43): libicu69-69.1-1.el8.remi.x86_64.rpm                                  1.9 MB/s | 9.6 MB     00:05
--------------------------------------------------------------------------------------------------------------
Total                                                                         3.0 MB/s |  28 MB     00:09
Extra Packages for Enterprise Linux 8 - x86_64                                1.6 MB/s | 1.6 kB     00:00
Importing GPG key 0x2F86D6A1:
 Userid     : "Fedora EPEL (8) <epel@fedoraproject.org>"
 Fingerprint: 94E2 79EB 8D8F 25B2 1810 ADF1 21EA 45AB 2F86 D6A1
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-8
Is this ok [y/N]: y
Key imported successfully
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                    3.0 MB/s | 3.1 kB     00:00
Importing GPG key 0x5F11735A:
 Userid     : "Remi's RPM repository <remi@remirepo.net>"
 Fingerprint: 6B38 FEA7 231F 87F5 2B9C A9D8 5550 9759 5F11 735A
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-remi.el8
Is this ok [y/N]: y
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                      1/1
  Installing       : libjpeg-turbo-1.5.3-12.el8.x86_64                                                   1/43
  Installing       : oniguruma5php-6.9.7.1-1.el8.remi.x86_64                                             2/43
  Installing       : libicu69-69.1-1.el8.remi.x86_64                                                     3/43
  Installing       : libsodium-1.0.18-2.el8.x86_64                                                       4/43
  Installing       : tcl-1:8.6.8-2.el8.x86_64                                                            5/43
  Running scriptlet: tcl-1:8.6.8-2.el8.x86_64                                                            5/43
  Installing       : environment-modules-4.5.2-1.el8.x86_64                                              6/43
  Running scriptlet: environment-modules-4.5.2-1.el8.x86_64                                              6/43
  Installing       : scl-utils-1:2.0.2-14.el8.x86_64                                                     7/43
  Installing       : python3-setools-4.3.0-2.el8.x86_64                                                  8/43
  Installing       : python3-libsemanage-2.9-6.el8.x86_64                                                9/43
  Installing       : python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64                             10/43
  Installing       : libxslt-1.1.32-6.el8.x86_64                                                        11/43
  Installing       : fontconfig-2.13.1-4.el8.x86_64                                                     12/43
  Running scriptlet: fontconfig-2.13.1-4.el8.x86_64                                                     12/43
  Installing       : checkpolicy-2.9-1.el8.x86_64                                                       13/43
  Installing       : python3-policycoreutils-2.9-16.el8.noarch                                          14/43
  Installing       : policycoreutils-python-utils-2.9-16.el8.noarch                                     15/43
  Installing       : php74-runtime-1.0-3.el8.remi.x86_64                                                16/43
  Running scriptlet: php74-runtime-1.0-3.el8.remi.x86_64                                                16/43
  Installing       : php74-php-json-7.4.26-1.el8.remi.x86_64                                            17/43
  Installing       : php74-php-common-7.4.26-1.el8.remi.x86_64                                          18/43
  Installing       : php74-php-pdo-7.4.26-1.el8.remi.x86_64                                             19/43
  Installing       : php74-php-cli-7.4.26-1.el8.remi.x86_64                                             20/43
  Installing       : php74-php-fpm-7.4.26-1.el8.remi.x86_64                                             21/43
  Running scriptlet: php74-php-fpm-7.4.26-1.el8.remi.x86_64                                             21/43
  Installing       : php74-php-mbstring-7.4.26-1.el8.remi.x86_64                                        22/43
  Installing       : php74-php-opcache-7.4.26-1.el8.remi.x86_64                                         23/43
  Installing       : php74-php-sodium-7.4.26-1.el8.remi.x86_64                                          24/43
  Installing       : php74-php-xml-7.4.26-1.el8.remi.x86_64                                             25/43
  Installing       : php74-libzip-1.8.0-1.el8.remi.x86_64                                               26/43
  Installing       : libwebp-1.0.0-5.el8.x86_64                                                         27/43
  Installing       : libXau-1.0.9-3.el8.x86_64                                                          28/43
  Installing       : libxcb-1.13.1-1.el8.x86_64                                                         29/43
  Installing       : libX11-common-1.6.8-5.el8.noarch                                                   30/43
  Installing       : libX11-1.6.8-5.el8.x86_64                                                          31/43
  Installing       : libXpm-3.5.12-8.el8.x86_64                                                         32/43
  Installing       : jbigkit-libs-2.1-14.el8.x86_64                                                     33/43
  Running scriptlet: jbigkit-libs-2.1-14.el8.x86_64                                                     33/43
  Installing       : libtiff-4.0.9-20.el8.x86_64                                                        34/43
  Installing       : gd-2.2.5-7.el8.x86_64                                                              35/43
  Running scriptlet: gd-2.2.5-7.el8.x86_64                                                              35/43
  Installing       : php74-php-gd-7.4.26-1.el8.remi.x86_64                                              36/43
  Installing       : php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64                                        37/43
  Installing       : php74-php-7.4.26-1.el8.remi.x86_64                                                 38/43
  Installing       : php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64                                         39/43
  Installing       : php74-php-bcmath-7.4.26-1.el8.remi.x86_64                                          40/43
  Installing       : php74-php-gmp-7.4.26-1.el8.remi.x86_64                                             41/43
  Installing       : php74-php-intl-7.4.26-1.el8.remi.x86_64                                            42/43
  Installing       : php74-php-process-7.4.26-1.el8.remi.x86_64                                         43/43
  Running scriptlet: php74-php-process-7.4.26-1.el8.remi.x86_64                                         43/43
  Running scriptlet: fontconfig-2.13.1-4.el8.x86_64                                                     43/43
  Running scriptlet: php74-php-fpm-7.4.26-1.el8.remi.x86_64                                             43/43
  Verifying        : gd-2.2.5-7.el8.x86_64                                                               1/43
  Verifying        : jbigkit-libs-2.1-14.el8.x86_64                                                      2/43
  Verifying        : libX11-1.6.8-5.el8.x86_64                                                           3/43
  Verifying        : libX11-common-1.6.8-5.el8.noarch                                                    4/43
  Verifying        : libXau-1.0.9-3.el8.x86_64                                                           5/43
  Verifying        : libXpm-3.5.12-8.el8.x86_64                                                          6/43
  Verifying        : libjpeg-turbo-1.5.3-12.el8.x86_64                                                   7/43
  Verifying        : libtiff-4.0.9-20.el8.x86_64                                                         8/43
  Verifying        : libwebp-1.0.0-5.el8.x86_64                                                          9/43
  Verifying        : libxcb-1.13.1-1.el8.x86_64                                                         10/43
  Verifying        : scl-utils-1:2.0.2-14.el8.x86_64                                                    11/43
  Verifying        : checkpolicy-2.9-1.el8.x86_64                                                       12/43
  Verifying        : environment-modules-4.5.2-1.el8.x86_64                                             13/43
  Verifying        : fontconfig-2.13.1-4.el8.x86_64                                                     14/43
  Verifying        : libxslt-1.1.32-6.el8.x86_64                                                        15/43
  Verifying        : policycoreutils-python-utils-2.9-16.el8.noarch                                     16/43
  Verifying        : python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64                             17/43
  Verifying        : python3-libsemanage-2.9-6.el8.x86_64                                               18/43
  Verifying        : python3-policycoreutils-2.9-16.el8.noarch                                          19/43
  Verifying        : python3-setools-4.3.0-2.el8.x86_64                                                 20/43
  Verifying        : tcl-1:8.6.8-2.el8.x86_64                                                           21/43
  Verifying        : libsodium-1.0.18-2.el8.x86_64                                                      22/43
  Verifying        : libicu69-69.1-1.el8.remi.x86_64                                                    23/43
  Verifying        : oniguruma5php-6.9.7.1-1.el8.remi.x86_64                                            24/43
  Verifying        : php74-libzip-1.8.0-1.el8.remi.x86_64                                               25/43
  Verifying        : php74-php-7.4.26-1.el8.remi.x86_64                                                 26/43
  Verifying        : php74-php-bcmath-7.4.26-1.el8.remi.x86_64                                          27/43
  Verifying        : php74-php-cli-7.4.26-1.el8.remi.x86_64                                             28/43
  Verifying        : php74-php-common-7.4.26-1.el8.remi.x86_64                                          29/43
  Verifying        : php74-php-fpm-7.4.26-1.el8.remi.x86_64                                             30/43
  Verifying        : php74-php-gd-7.4.26-1.el8.remi.x86_64                                              31/43
  Verifying        : php74-php-gmp-7.4.26-1.el8.remi.x86_64                                             32/43
  Verifying        : php74-php-intl-7.4.26-1.el8.remi.x86_64                                            33/43
  Verifying        : php74-php-json-7.4.26-1.el8.remi.x86_64                                            34/43
  Verifying        : php74-php-mbstring-7.4.26-1.el8.remi.x86_64                                        35/43
  Verifying        : php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64                                         36/43
  Verifying        : php74-php-opcache-7.4.26-1.el8.remi.x86_64                                         37/43
  Verifying        : php74-php-pdo-7.4.26-1.el8.remi.x86_64                                             38/43
  Verifying        : php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64                                        39/43
  Verifying        : php74-php-process-7.4.26-1.el8.remi.x86_64                                         40/43
  Verifying        : php74-php-sodium-7.4.26-1.el8.remi.x86_64                                          41/43
  Verifying        : php74-php-xml-7.4.26-1.el8.remi.x86_64                                             42/43
  Verifying        : php74-runtime-1.0-3.el8.remi.x86_64                                                43/43

Installed:
  checkpolicy-2.9-1.el8.x86_64                      environment-modules-4.5.2-1.el8.x86_64
  fontconfig-2.13.1-4.el8.x86_64                    gd-2.2.5-7.el8.x86_64
  jbigkit-libs-2.1-14.el8.x86_64                    libX11-1.6.8-5.el8.x86_64
  libX11-common-1.6.8-5.el8.noarch                  libXau-1.0.9-3.el8.x86_64
  libXpm-3.5.12-8.el8.x86_64                        libicu69-69.1-1.el8.remi.x86_64
  libjpeg-turbo-1.5.3-12.el8.x86_64                 libsodium-1.0.18-2.el8.x86_64
  libtiff-4.0.9-20.el8.x86_64                       libwebp-1.0.0-5.el8.x86_64
  libxcb-1.13.1-1.el8.x86_64                        libxslt-1.1.32-6.el8.x86_64
  oniguruma5php-6.9.7.1-1.el8.remi.x86_64           php74-libzip-1.8.0-1.el8.remi.x86_64
  php74-php-7.4.26-1.el8.remi.x86_64                php74-php-bcmath-7.4.26-1.el8.remi.x86_64
  php74-php-cli-7.4.26-1.el8.remi.x86_64            php74-php-common-7.4.26-1.el8.remi.x86_64
  php74-php-fpm-7.4.26-1.el8.remi.x86_64            php74-php-gd-7.4.26-1.el8.remi.x86_64
  php74-php-gmp-7.4.26-1.el8.remi.x86_64            php74-php-intl-7.4.26-1.el8.remi.x86_64
  php74-php-json-7.4.26-1.el8.remi.x86_64           php74-php-mbstring-7.4.26-1.el8.remi.x86_64
  php74-php-mysqlnd-7.4.26-1.el8.remi.x86_64        php74-php-opcache-7.4.26-1.el8.remi.x86_64
  php74-php-pdo-7.4.26-1.el8.remi.x86_64            php74-php-pecl-zip-1.20.0-1.el8.remi.x86_64
  php74-php-process-7.4.26-1.el8.remi.x86_64        php74-php-sodium-7.4.26-1.el8.remi.x86_64
  php74-php-xml-7.4.26-1.el8.remi.x86_64            php74-runtime-1.0-3.el8.remi.x86_64
  policycoreutils-python-utils-2.9-16.el8.noarch    python3-audit-3.0-0.17.20191104git1c2f876.el8.1.x86_64
  python3-libsemanage-2.9-6.el8.x86_64              python3-policycoreutils-2.9-16.el8.noarch
  python3-setools-4.3.0-2.el8.x86_64                scl-utils-1:2.0.2-14.el8.x86_64
  tcl-1:8.6.8-2.el8.x86_64

Complete!
```
## 2. Conf Apache
**Analyser la conf Apache**
**Créer un VirtualHost qui accueillera NextCloud**

- créez un nouveau fichier dans le dossier de *drop-in*
  - attention, il devra être correctement nommé (l'extension) pour être inclus par le fichier de conf principal
- ce fichier devra avoir le contenu suivant :

```apache
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/  

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp5.linux  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

> N'oubliez pas de redémarrer le service à chaque changement de la configuration, pour que les changements prennent effet.

🌞 **Configurer la racine web**

- la racine Web, on l'a configurée dans Apache pour être le dossier `/var/www/nextcloud/html/`
- creéz ce dossier
- faites appartenir le dossier et son contenu à l'utilisateur qui lance Apache (commande `chown`, voir le [mémo commandes](../../cours/memos/commandes.md))

> Jusqu'à la fin du TP, tout le contenu de ce dossier doit appartenir à l'utilisateur qui lance Apache. C'est strictement nécessaire pour qu'Apache puisse lire le contenu, et le servir aux clients.

🌞 **Configurer PHP**

- dans l'install de NextCloud, PHP a besoin de conaître votre timezone (fuseau horaire)
- pour récupérer la timezone actuelle de la machine, utilisez la commande `timedatectl` (sans argument)
- modifiez le fichier `/etc/opt/remi/php74/php.ini` :
  - changez la ligne `;date.timezone =`
  - par `date.timezone = "<VOTRE_TIMEZONE>"`
  - par exemple `date.timezone = "Europe/Paris"`

## 3. Install NextCloud

On dit "installer NextCloud" mais en fait c'est juste récupérer les fichiers PHP, HTML, JS, etc... qui constituent NextCloud, et les mettre dans le dossier de la racine web.

🌞 **Récupérer Nextcloud**

```bash
# Petit tips : la commande cd sans argument permet de retourner dans votre homedir
$ cd

# La commande curl -SLO permet de rapidement télécharger un fichier, en HTTP/HTTPS, dans le dossier courant
$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip

$ ls
nextcloud-21.0.1.zip
```

🌞 **Ranger la chambre**

- extraire le contenu de NextCloud (beh ui on a récup un `.zip`)
- déplacer tout le contenu dans la racine Web
  - n'oubliez pas de gérer les permissions de tous les fichiers déplacés ;)
- supprimer l'archive

## 4. Test

Bah on arrive sur la fin !

Si on résume :

- **un serveur de base de données : `db.tp5.linux`**
  - MariaDB installé et fonctionnel
  - firewall configuré
  - une base de données et un user pour NextCloud ont été créés dans MariaDB
- **un serveur Web : `web.tp5.linux`**
  - Apache installé et fonctionnel
  - firewall configuré
  - un VirtualHost qui pointe vers la racine `/var/www/nextcloud/html/`
  - NextCloud installé dans le dossier `/var/www/nextcloud/html/`

**Looks like we're ready.**

---

**Ouuu presque. Pour que NextCloud fonctionne correctement, il faut y accéder en utilisant un nom, et pas une IP.**  
On va donc devoir faire en sorte que, depuis votre PC, vous puissiez écrire `http://web.tp5.linux` plutôt que `http://10.5.1.11`.

➜ Pour faire ça, on va utiliser **le fichier `hosts`**. C'est un fichier présents sur toutes les machines, sur tous les OS.  
Il sert à définir, localement, une correspondance entre une IP et un ou plusieurs noms.  

C'est arbitraire, on fait ce qu'on veut.  
Si on veut que `www.ynov.com` pointe vers le site de notre VM, ou vers n'importe quelle autre IP, on peut.  
ON PEUT TOUT FAIRE JE TE DIS.  
Ce sera évidemment valable uniquement sur la machine où se trouve le fichier.

Emplacement du fichier `hosts` :

- MacOS/Linux : `/etc/hosts`
- Windows : `c:\windows\system32\drivers\etc\hosts`

---

🌞 **Modifiez le fichier `hosts` de votre PC**

- ajoutez la ligne : `10.5.1.11 web.tp5.linux`

🌞 **Tester l'accès à NextCloud et finaliser son install'**

- ouvrez votre navigateur Web sur votre PC
- rdv à l'URL `http://web.tp5.linux`
- vous devriez avoir la page d'accueil de NextCloud
- ici deux choses :
  - les deux champs en haut pour créer un user admin au sein de NextCloud
  - le bouton "Configure the database" en bas
    - sélectionnez "MySQL/MariaDB"
    - entrez les infos pour que NextCloud sache comment se connecter à votre serveur de base de données
    - c'est les infos avec lesquelles vous avez validé à la main le bon fonctionnement de MariaDB (c'était avec la commande `mysql`)

---
