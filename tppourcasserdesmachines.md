# TP LINUX 1 


## METHODE POUR CASSER UNE VM H2


1.**Faire paniquer le noyau**  
    sudo dd if=/dev/random of=/dev/port 
        obliger d'utiliser sudo partout sinon ça ne marche pas  

2.**Fait apparaitre des messages en boucle empechant d'ecrire**  
    :(){ :|: & };:  
        Fork bomb (toujours pas compris le rapport avec une fourchette)  

3.**Supprime tout jusqu'à la racine**  
    sudo rm -rf /*  

4.**Efface le disque**  
    sudo dd if=/dev/zero of=/dev/sda  

5.**Ecrase tout**  
    sudo dd if=/dev/random of=/dev/sda  
