# TP4 : Une distribution orientée serveur

# I. Checklist

**Choisissez et définissez une IP à la VM**

```bash
TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.200.1.37
NETMASK=255.255.255.0
```
```bash
[val@localhost ~]$ [val@localhost ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:aa:a0:55 brd ff:ff:ff:ff:ff:ff
    inet 10.200.1.37/24 brd 10.200.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feaa:a055/64 scope link
       valid_lft forever preferred_lft forever
```

```bash
[val@localhost ~]$ systemctl  status
● localhost.localdomain
    State: degraded
     Jobs: 0 queued
   Failed: 1 units
    Since: Wed 2021-11-24 17:49:27 CET; 6min ago
   CGroup: /
           ├─user.slice
           │ └─user-1000.slice
           │   ├─user@1000.service
           │   │ └─init.scope
           │   │   ├─1431 /usr/lib/systemd/systemd --user
           │   │   └─1435 (sd-pam)
           │   ├─session-3.scope
           │   │ ├─1470 sshd: val [priv]
           │   │ ├─1474 sshd: val@pts/0
           │   │ ├─1475 -bash
           │   │ ├─1558 systemctl status
           │   │ └─1559 systemctl status
           │   └─session-1.scope
           │     ├─ 865 login -- val
           │     └─1442 -bash
           ├─init.scope
           │ └─1 /usr/lib/systemd/systemd --switched-root --system --deserialize 17
           └─system.slice
             ├─libstoragemgmt.service
             │ └─812 /usr/bin/lsmd -d
             ├─systemd-udevd.service
             │ └─685 /usr/lib/systemd/systemd-udevd
             ├─polkit.service
             │ └─810 /usr/lib/polkit-1/polkitd --no-debug
             ├─auditd.service
             │ └─783 /sbin/auditd
             ├─tuned.service
             │ └─855 /usr/libexec/platform-python -Es /usr/sbin/tuned -l -P
             ├─systemd-journald.service
             │ └─657 /usr/lib/systemd/systemd-journald
             ├─atd.service
             │ └─864 /usr/sbin/atd -f
             ├─sshd.service
             │ └─854 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-poly1305@openssh.com,aes256-ctr,aes256>
             ├─crond.service
             │ └─867 /usr/sbin/crond -n
             ├─NetworkManager.service
             │ └─840 /usr/sbin/NetworkManager --no-daemon
             ├─firewalld.service
             │ └─824 /usr/libexec/platform-python -s /usr/sbin/firewalld --nofork --nopid
             ├─sssd.service
             │ ├─811 /usr/sbin/sssd -i --logger=files
             │ ├─821 /usr/libexec/sssd/sssd_be --domain implicit_files --uid 0 --gid 0 --logger=files
```
```bash
PS C:\Users\bravo\.ssh> type .\id_rsa.pub
```
```bash
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDcGOTS+28XZUmNM9kU8e1YI7OztrtgyK4x6GvhwOTbbWK7ud+++aMnJgD4xNAuljjcKajBwPfZN0hJKAOyxU4xMwEohFTqG0SHvNkZoCeWFLvpfktNzyNxLAxl/cG2cmrNpczo13LEceas4b8kgHnTsQIy3K6INFTz8h0vJXQeC/1BCQjp+MmCBOaB0QB++ui8Mmu6OSa1fpEZCwVAF53MpWKpRshRwQxJzLdS/d6F58xNTh9MYFxCwfpFH8uSRYHFpiwtBIcemgipdYEZXC8JKdiy7R6zPMguYg48nlAx5iEFW3u2Bu1mPPlu8TrDIryMJY0dW/CZxXzQPvuwuVrNmCGewOdvBebD5a1vX93h3iDWx7SCt41bgQh4mwibj1ZPNZQngnu9SnT/H8+kaV734JCyy68Zyx8UMzqAwPFnFNglAxncAD+XjolzeeC0eIekRiGxHT9x9o46T5ub+kViHc4AhTq19sK1FkuHpO7l/y7o6IqEjWA7u71pE3ExVSX/08MQrQcaQFHTU1B0i2B055GSCEKDn+7IB4igglwt9CtnkWmpW6rX85jL0hvBRANRlLwrO63vQ0QnCwD7EkPQ35Oyl1DM/yDHfGlohnYesTC7E6KFl3/ceFtwqY7FLYH/NEORWljv5Je2EQAziDBR8yVFSQg8P+Blmt1n3hH3Fw== bravo@Asus-Valentin
```
```bash
PS C:\Users\bravo> ssh val@10.200.1.37
```
```bash
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Nov 24 18:41:22 2021 from 10.200.1.1
[val@localhost ~]$ cat .ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDcGOTS+28XZUmNM9kU8e1YI7OztrtgyK4x6GvhwOTbbWK7ud+++aMnJgD4xNAuljjcKajBwPfZN0hJKAOyxU4xMwEohFTqG0SHvNkZoCeWFLvpfktNzyNxLAxl/cG2cmrNpczo13LEceas4b8kgHnTsQIy3K6INFTz8h0vJXQeC/1BCQjp+MmCBOaB0QB++ui8Mmu6OSa1fpEZCwVAF53MpWKpRshRwQxJzLdS/d6F58xNTh9MYFxCwfpFH8uSRYHFpiwtBIcemgipdYEZXC8JKdiy7R6zPMguYg48nlAx5iEFW3u2Bu1mPPlu8TrDIryMJY0dW/CZxXzQPvuwuVrNmCGewOdvBebD5a1vX93h3iDWx7SCt41bgQh4mwibj1ZPNZQngnu9SnT/H8+kaV734JCyy68Zyx8UMzqAwPFnFNglAxncAD+XjolzeeC0eIekRiGxHT9x9o46T5ub+kViHc4AhTq19sK1FkuHpO7l/y7o6IqEjWA7u71pE3ExVSX/08MQrQcaQFHTU1B0i2B055GSCEKDn+7IB4igglwt9CtnkWmpW6rX85jL0hvBRANRlLwrO63vQ0QnCwD7EkPQ35Oyl1DM/yDHfGlohnYesTC7E6KFl3/ceFtwqY7FLYH/NEORWljv5Je2EQAziDBR8yVFSQg8P+Blmt1n3hH3Fw== bravo@Asus-Valentin
```
```bash
PS C:\Users\bravo> ssh val@10.200.1.37
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Wed Nov 24 18:41:22 2021 from 10.200.1.1
```

➜ **Accès internet**

**Prouvez que vous avez un accès internet**
```bash
[val@localhost ~]$ ping 1.1.1.1
```
```bash
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=59 time=14.3 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=59 time=43.6 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=59 time=15.5 ms
64 bytes from 1.1.1.1: icmp_seq=4 ttl=59 time=17.1 ms
^C
--- 1.1.1.1 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3022ms
rtt min/avg/max/mdev = 14.290/22.610/43.629/12.175 ms
```

**Prouvez que vous avez de la résolution de nom**
```bash
[val@localhost ~]$ ping google.com
PING google.com (216.58.213.78) 56(84) bytes of data.
64 bytes from lhr25s01-in-f78.1e100.net (216.58.213.78): icmp_seq=1 ttl=119 time=16.1 ms
64 bytes from lhr25s01-in-f78.1e100.net (216.58.213.78): icmp_seq=2 ttl=119 time=15.4 ms
64 bytes from lhr25s01-in-f78.1e100.net (216.58.213.78): icmp_seq=3 ttl=119 time=14.7 ms
^C
--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 14.728/15.384/16.051/0.549 ms
```

➜ **Nommage de la machine**

```bash
[val@localhost ~]$ [val@localhost ~]$ hostname
node1.tp4.linux
```
```bash
[val@localhost ~]$ sudo nano /etc/hostname
localhost.localdomain
```

# III. Mettre en place un service

## 3. Analyse

```bash
$ sudo systemctl start nginx
$ sudo systemctl status nginx
```

**Analysez le service NGINX**
```bash
[val@localhost ~]$ ps -aux | grep nginx
root        4751  0.0  0.2 119160  2164 ?        Ss   19:00   0:00 nginx: master process /usr/sbin/nginx
```
```bash
[val@localhost ~]$ ss | grep ssh
tcp   ESTAB  0      36                                               10.200.1.37:ssh         10.200.1.1:59694
```

## 4. Visite du service web

**Configurez le firewall pour autoriser le trafic vers le service NGINX** 

**Tester le bon fonctionnement du service**

```bash
Ce site est inaccessible
10.200.1.37 a mis trop de temps à répondre.
```
## 5. Modif de la conf du serveur web

**Changer le port d'écoute**
j'ai modifié sshd_config du
```bash
# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
Port 8080
```
mais ça marche pas

🌞 **Changer l'utilisateur qui lance le service**

- pour ça, vous créerez vous-même un nouvel utilisateur sur le système : `web`
  - référez-vous au [mémo des commandes](../../cours/memos/commandes.md) pour la création d'utilisateur
  - l'utilisateur devra avoir un mot de passe, et un homedir défini explicitement à `/home/web`
- un peu de conf à modifier dans le fichier de conf de NGINX pour définir le nouvel utilisateur en tant que celui qui lance le service
  - vous me montrerez la conf effectuée dans le compte-rendu
- n'oubliez pas de redémarrer le service pour que le changement prenne effet
- vous prouverez avec une commande `ps` que le service tourne bien sous ce nouveau utilisateur

---

🌞 **Changer l'emplacement de la racine Web**

- vous créerez un nouveau dossier : `/var/www/super_site_web`
  - avec un fichier  `/var/www/super_site_web/index.html` qui contient deux trois lignes de HTML, peu importe, un bon `<h1>toto</h1>` des familles, ça fera l'affaire
  - le dossier et tout son contenu doivent appartenir à `web`
- configurez NGINX pour qu'il utilise cette nouvelle racine web
  - vous me montrerez la conf effectuée dans le compte-rendu
- n'oubliez pas de redémarrer le service pour que le changement prenne effet
- prouvez avec un `curl` depuis votre hôte que vous accédez bien au nouveau site

![Uuuuunpossible](./pics/nginx_unpossible.jpg)
