Name=$(hostname)
echo "Machine name : $Name"
OS=$(cat /etc/issue | cut -d" " -f1,2)
NOYAU=$(cat /proc/version | cut -d" " -f1,2,3)
echo "OS $OS and kernel version is $NOYAU"
IP=$(ip a | grep inet | grep -v  "link" | grep -v "inet6" | grep -v "enp0s3" | grep -v "host" | cut -d" " -f6)
echo "IP : $IP"
RAM=$(free -h | grep Mem | tr -s " " |cut -d" " -f7)
REM=$(free -h | grep Mem | tr -s " " |cut -d" " -f2)
echo "RAM : $RAM de RAM restante sur $REM de RAM totale"
RENAULT=$(df -h | grep /dev/sda5 |tr -s " " |cut -d" " -f2)
echo "Disque : $RENAULT space left"
PROCESS=$(ps -o pid,%mem,command ax | sort -b -k2 -r | head -6 | grep -v PID)
echo "Top 5 processes by RAM usage :"
echo "$PROCESS"
iter=$(sudo ss -alnpt | grep -v State | tr -s " ")
counter=$(sudo ss -alnpt | grep -v State | tr -s " " |cut -d" " -f3 | wc -l)
echo "Listening ports :"
for ((i = 1; i < $counter+1 ; i++))
do
PORT=$(sudo ss -alnpt | grep -v State | tr -s " " |cut -d" " -f3  | head -$i | tail -1)
PROC=$(sudo ss -alnpt | grep -v State | tr -s " " |cut -d" " -f6 | cut -d'"' -f2 | head -$i | tail -1) 
echo "$PROC : $PORT"
done
image=$(curl https://api.thecatapi.com/v1/images/search 2> /dev/null | awk -F "http" '{print $2}' | cut -d '"' -f1)
echo "Here is your kitty cat picture" http$image 
