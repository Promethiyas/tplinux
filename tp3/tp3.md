# TP 3 : A little script
## I. Script carte d'identité
val@node1:/srv/idcard$ sudo bash idcard.sh
```bash
Machine name : node1.tp2.linux
OS Ubuntu 20.04.3 and kernel version is Linux version 5.11.0-38-generic
IP : 192.168.56.110/24
RAM : 509Mi de RAM restante sur 978Mi de RAM totale
Disque : 9,3G space left
Top 5 processes by RAM usage :
   1474  2.3 update-notifier
    858  2.0 /usr/lib/xorg/Xorg vt2 -displayfd 3 -auth /run/user/1000/gdm/Xauthority -background none -noreset -keeptty -verbose 3
   1132 14.0 /usr/bin/gnome-shell
    213  1.3 /lib/systemd/systemd-journald
    602  1.0 /usr/lib/snapd/snapd
Listening ports :
systemd-resolve : 4096
cupsd : 5
sshd : 128
vsftpd : 32
cupsd : 5
sshd : 128
Here is your kitty cat picture https://cdn2.thecatapi.com/images/pib_sfrX8.jpg
```
