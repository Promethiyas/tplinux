# Partie 1 : Préparation de la machine `backup.tp6.linux`
# I. Ajout de disque

**Ajouter un disque dur de 5Go à la VM `backup.tp6.linux`**
```bash
[val@backup ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    8G  0 disk
sr0          11:0    1 1024M  0 rom
```
```bash
sdb           8:16   0    8G  0 disk
```
# II. Partitioning
**Partitionner le disque à l'aide de LVM**
```bash
[val@backup ~]$ sudo pvcreate /dev/sdb
[sudo] password for val:
  Physical volume "/dev/sdb" successfully created.
[val@backup ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               rl
  PV Size               <7.00 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              1791
  Free PE               0
  Allocated PE          1791
  PV UUID               X5kY0o-sxic-gGtZ-ONvL-sxUw-cQJ9-fMPLY7

  "/dev/sdb" is a new physical volume of "8.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               8.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               4xOOA1-skaa-eXr3-auNd-R1UB-5KMU-s3ZSMg
  ```
```bash
[val@backup ~]$ sudo vgcreate backup /dev/sdb
[sudo] password for val:
  Volume group "backup" successfully created
[val@backup ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               backup
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <8.00 GiB
  PE Size               4.00 MiB
  Total PE              2047
  Alloc PE / Size       0 / 0
  Free  PE / Size       2047 / <8.00 GiB
  VG UUID               3sPm0A-0BIF-2tyG-wlrS-7352-dbpd-CqnwAT
```
```bash
[val@backup ~]$ sudo lvcreate -l 100%FREE backup -n lv
  Logical volume "lv" created.
[val@backup ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/backup/lv
  LV Name                lv
  VG Name                backup
  LV UUID                tSoSeY-CVxJ-v19M-vwE7-flRo-j3RB-PvKL5b
  LV Write Access        read/write
  LV Creation host, time backup.tp6.linux, 2021-11-30 16:57:38 +0100
  LV Status              available
  # open                 0
  LV Size                <8.00 GiB
  Current LE             2047
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2
  ```
**Formater la partition**
```bash
[val@backup ~]$ sudo mkfs -t ext4 /dev/backup/lv
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 2096128 4k blocks and 524288 inodes
Filesystem UUID: e6de2ade-292e-4ccb-8481-331d78c9d98a
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
```
**Monter la partition**
```bash
[val@backup ~]$ sudo mount /dev/backup/lv /dev/backup
```
```bash
[val@backup ~]$ df -h | grep backup
/dev/mapper/backup-lv  7.9G   36M  7.4G   1% /dev/backup
```
```bash
[val@backup ~]$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Tue Nov 23 14:47:24 2021
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=73840b3b-21bd-47e5-bfe8-d91cebc1ccb4 /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
/dev/backup/lv /mnt/data1 ext4 defaults 0 0
```
# Partie 2 : Setup du serveur NFS sur `backup.tp6.linux`
**Préparer les dossiers à partager**
```bash
[val@backup backup]$ ls
db.tp6.linux  lost+found  web.tp6.linux
```
**Install du serveur NFS**
```bash
[val@backup ~]$ sudo dnf install nfs-utils
[sudo] password for val:
Last metadata expiration check: 0:43:45 ago on Wed 01 Dec 2021 10:23:02 AM CET.
Dependencies resolved.
========================================================================================================================
 Package                           Architecture           Version                          Repository              Size
========================================================================================================================
Installing:
 nfs-utils                         x86_64                 1:2.3.3-46.el8                   baseos                 499 k
Installing dependencies:
 gssproxy                          x86_64                 0.8.0-19.el8                     baseos                 118 k
 keyutils                          x86_64                 1.5.10-9.el8                     baseos                  65 k
 libevent                          x86_64                 2.1.8-5.el8                      baseos                 252 k
 libverto-libevent                 x86_64                 0.3.0-5.el8                      baseos                  15 k
 rpcbind                           x86_64                 1.2.5-8.el8                      baseos                  69 k

Transaction Summary
========================================================================================================================
Install  6 Packages

Total download size: 1.0 M
Installed size: 2.9 M
Is this ok [y/N]: y
Downloading Packages:
(1/6): gssproxy-0.8.0-19.el8.x86_64.rpm                                                 133 kB/s | 118 kB     00:00
(2/6): libverto-libevent-0.3.0-5.el8.x86_64.rpm                                          12 kB/s |  15 kB     00:01
(3/6): libevent-2.1.8-5.el8.x86_64.rpm                                                  121 kB/s | 252 kB     00:02
(4/6): rpcbind-1.2.5-8.el8.x86_64.rpm                                                   275 kB/s |  69 kB     00:00
(5/6): keyutils-1.5.10-9.el8.x86_64.rpm                                                  14 kB/s |  65 kB     00:04
(6/6): nfs-utils-2.3.3-46.el8.x86_64.rpm                                                 73 kB/s | 499 kB     00:06
------------------------------------------------------------------------------------------------------------------------
Total                                                                                    70 kB/s | 1.0 MB     00:14
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : libevent-2.1.8-5.el8.x86_64                                                                    1/6
  Installing       : libverto-libevent-0.3.0-5.el8.x86_64                                                           2/6
  Installing       : gssproxy-0.8.0-19.el8.x86_64                                                                   3/6
  Running scriptlet: gssproxy-0.8.0-19.el8.x86_64                                                                   3/6
  Running scriptlet: rpcbind-1.2.5-8.el8.x86_64                                                                     4/6
  Installing       : rpcbind-1.2.5-8.el8.x86_64                                                                     4/6
  Running scriptlet: rpcbind-1.2.5-8.el8.x86_64                                                                     4/6
  Installing       : keyutils-1.5.10-9.el8.x86_64                                                                   5/6
  Running scriptlet: nfs-utils-1:2.3.3-46.el8.x86_64                                                                6/6
  Installing       : nfs-utils-1:2.3.3-46.el8.x86_64                                                                6/6
  Running scriptlet: nfs-utils-1:2.3.3-46.el8.x86_64                                                                6/6
  Verifying        : gssproxy-0.8.0-19.el8.x86_64                                                                   1/6
  Verifying        : keyutils-1.5.10-9.el8.x86_64                                                                   2/6
  Verifying        : libevent-2.1.8-5.el8.x86_64                                                                    3/6
  Verifying        : libverto-libevent-0.3.0-5.el8.x86_64                                                           4/6
  Verifying        : nfs-utils-1:2.3.3-46.el8.x86_64                                                                5/6
  Verifying        : rpcbind-1.2.5-8.el8.x86_64                                                                     6/6

Installed:
  gssproxy-0.8.0-19.el8.x86_64                keyutils-1.5.10-9.el8.x86_64           libevent-2.1.8-5.el8.x86_64
  libverto-libevent-0.3.0-5.el8.x86_64        nfs-utils-1:2.3.3-46.el8.x86_64        rpcbind-1.2.5-8.el8.x86_64

Complete!
```
**Conf du serveur NFS**
```bash
[val@backup ~]$ cat /etc/idmapd.conf | grep Domain
Domain = tp6.linux
```
```bash
[val@backup ~]$ cat /etc/exports
/backup/web.tp6.linux/ 10.5.1.5/24(rw,no_root_squash)
/backup/db.tp6.linux/ 10.5.1.5/24(rw,no_root_squash)
```
**Vous expliquerez ce que signifient ces deux-là.**  
**rw**   
read ad write? genre on peut lire les fichiers et les modifier et en créer?  
**no_root_squash**  
Les utilisateurs root emote peuvent modifier n'importe quel fichier sur le système de fichiers partagé et laisser les applications trojanes pour que d'autres utilisateurs s'exécutent par inadvertance  

**Démarrez le service**
```bash
[val@backup ~]$ sudo systemctl start nfs-server
[sudo] password for val:
[val@backup ~]$ sudo systemctl status nfs-server.service
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; disabled; vendor preset: disabled)
   Active: active (exited) since Wed 2021-12-01 13:40:42 CET; 13s ago
  Process: 25353 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl reload gssproxy ; fi (code=>
  Process: 25341 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
  Process: 25340 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=1/FAILURE)
 Main PID: 25353 (code=exited, status=0/SUCCESS)

Dec 01 13:40:42 backup.tp6.linux systemd[1]: Starting NFS server and services...
Dec 01 13:40:42 backup.tp6.linux exportfs[25340]: exportfs: Failed to stat /backup/db.tp6.linux/: No such file or di>
Dec 01 13:40:42 backup.tp6.linux exportfs[25340]: exportfs: Failed to stat /backup/web.tp6.linux/: No such file or d>
Dec 01 13:40:42 backup.tp6.linux systemd[1]: Started NFS server and services.

[val@backup ~]$ sudo systemctl enable nfs-server.service
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
```
**Firewall**
```bash
[val@backup ~]$ sudo firewall-cmd --zone=public --add-port=2049/tcp
[sudo] password for val:
success
```
# Partie 3 : Setup des clients NFS : `web.tp6.linux` et `db.tp6.linux`
**Install'**
```bash
[val@web ~]$ sudo dnf install nfs-utils
[sudo] password for val:
Rocky Linux 8 - AppStream                                                             11 kB/s | 4.8 kB     00:00
Rocky Linux 8 - AppStream                                                            7.2 MB/s | 8.3 MB     00:01
Rocky Linux 8 - BaseOS                                                                13 kB/s | 4.3 kB     00:00
Rocky Linux 8 - BaseOS                                                               2.9 MB/s | 3.5 MB     00:01
Rocky Linux 8 - Extras                                                               2.7 kB/s | 3.5 kB     00:01
Rocky Linux 8 - Extras                                                                11 kB/s |  10 kB     00:00
Extra Packages for Enterprise Linux 8 - x86_64                                        53 kB/s |  31 kB     00:00
Extra Packages for Enterprise Linux 8 - x86_64                                       3.3 MB/s |  11 MB     00:03
Extra Packages for Enterprise Linux Modular 8 - x86_64                                87 kB/s |  34 kB     00:00
Extra Packages for Enterprise Linux Modular 8 - x86_64                               761 kB/s | 980 kB     00:01
Remi's Modular repository for Enterprise Linux 8 - x86_64                            2.6 kB/s | 858  B     00:00
Remi's Modular repository for Enterprise Linux 8 - x86_64                            2.4 MB/s | 946 kB     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                           4.5 kB/s | 858  B     00:00
Safe Remi's RPM repository for Enterprise Linux 8 - x86_64                           4.2 MB/s | 2.0 MB     00:00
Last metadata expiration check: 0:00:01 ago on Tue 07 Dec 2021 12:53:52 PM CET.
Dependencies resolved.
=====================================================================================================================
 Package                          Architecture          Version                          Repository             Size
=====================================================================================================================
Installing:
 nfs-utils                        x86_64                1:2.3.3-46.el8                   baseos                499 k
Installing dependencies:
 gssproxy                         x86_64                0.8.0-19.el8                     baseos                118 k
 keyutils                         x86_64                1.5.10-9.el8                     baseos                 65 k
 libevent                         x86_64                2.1.8-5.el8                      baseos                252 k
 libverto-libevent                x86_64                0.3.0-5.el8                      baseos                 15 k
 rpcbind                          x86_64                1.2.5-8.el8                      baseos                 69 k

Transaction Summary
=====================================================================================================================
Install  6 Packages

Total download size: 1.0 M
Installed size: 2.9 M
Is this ok [y/N]: y
Downloading Packages:
(1/6): keyutils-1.5.10-9.el8.x86_64.rpm                                              263 kB/s |  65 kB     00:00
(2/6): gssproxy-0.8.0-19.el8.x86_64.rpm                                              444 kB/s | 118 kB     00:00
(3/6): libverto-libevent-0.3.0-5.el8.x86_64.rpm                                      382 kB/s |  15 kB     00:00
(4/6): libevent-2.1.8-5.el8.x86_64.rpm                                               784 kB/s | 252 kB     00:00
(5/6): rpcbind-1.2.5-8.el8.x86_64.rpm                                                1.0 MB/s |  69 kB     00:00
(6/6): nfs-utils-2.3.3-46.el8.x86_64.rpm                                             2.4 MB/s | 499 kB     00:00
---------------------------------------------------------------------------------------------------------------------
Total                                                                                1.2 MB/s | 1.0 MB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                             1/1
  Installing       : libevent-2.1.8-5.el8.x86_64                                                                 1/6
  Installing       : libverto-libevent-0.3.0-5.el8.x86_64                                                        2/6
  Installing       : gssproxy-0.8.0-19.el8.x86_64                                                                3/6
  Running scriptlet: gssproxy-0.8.0-19.el8.x86_64                                                                3/6
  Running scriptlet: rpcbind-1.2.5-8.el8.x86_64                                                                  4/6
  Installing       : rpcbind-1.2.5-8.el8.x86_64                                                                  4/6
  Running scriptlet: rpcbind-1.2.5-8.el8.x86_64                                                                  4/6
  Installing       : keyutils-1.5.10-9.el8.x86_64                                                                5/6
  Running scriptlet: nfs-utils-1:2.3.3-46.el8.x86_64                                                             6/6
  Installing       : nfs-utils-1:2.3.3-46.el8.x86_64                                                             6/6
  Running scriptlet: nfs-utils-1:2.3.3-46.el8.x86_64                                                             6/6
  Verifying        : gssproxy-0.8.0-19.el8.x86_64                                                                1/6
  Verifying        : keyutils-1.5.10-9.el8.x86_64                                                                2/6
  Verifying        : libevent-2.1.8-5.el8.x86_64                                                                 3/6
  Verifying        : libverto-libevent-0.3.0-5.el8.x86_64                                                        4/6
  Verifying        : nfs-utils-1:2.3.3-46.el8.x86_64                                                             5/6
  Verifying        : rpcbind-1.2.5-8.el8.x86_64                                                                  6/6

Installed:
  gssproxy-0.8.0-19.el8.x86_64               keyutils-1.5.10-9.el8.x86_64          libevent-2.1.8-5.el8.x86_64
  libverto-libevent-0.3.0-5.el8.x86_64       nfs-utils-1:2.3.3-46.el8.x86_64       rpcbind-1.2.5-8.el8.x86_64

Complete!
```
**Conf'**
```bash
[val@web srv]$ ls
backup
```
```bash
[val@web etc]$ cat idmapd.conf | grep Domain
Domain = tp6.linux
```
**Montage !**
```bash
[val@web ~]$ sudo mount -t nfs 10.5.1.13:/srv/backup/web.tp6.linux/ /srv/backup
```
  - la partition doit être montée sur le point de montage `/srv/backup`
  - preuve avec une commande `df -h` que la partition est bien montée
  - prouvez que vous pouvez lire et écrire des données sur cette partition
- définir un montage automatique de la partition (fichier `/etc/fstab`)
  - vous vérifierez que votre fichier `/etc/fstab` fonctionne correctement

---

🌞 **Répétez les opérations sur `db.tp6.linux`**

- le point de montage sur la machine `db.tp6.linux` est aussi sur `/srv/backup`
- le dossier à monter est `/backup/db.tp6.linux/`
- vous ne mettrez dans le compte-rendu pour `db.tp6.linux` que les preuves de fonctionnement :
  - preuve avec une commande `df -h` que la partition est bien montée
  - preuve que vous pouvez lire et écrire des données sur cette partition
  - preuve que votre fichier `/etc/fstab` fonctionne correctement
